﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using Swan.Logging;
using EmbedIO;
using EmbedIO.Actions;
using EmbedIO.WebApi;

namespace DukaDixaBridge
{
    public partial class Form1 : Form
    {
        private readonly BackgroundWorker serverWorker;


        public Form1()
        {
            var url = "https://+:443/";
            InitializeComponent();
            //label2.Text =  Application.StartupPath;

            serverWorker = new BackgroundWorker()
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true,
            };

            serverWorker.ProgressChanged += (s, e) =>
            {
                Console.WriteLine("Progress Changed");
                if (e.ProgressPercentage == 100)
                {
                }
                else
                {
                }

            };

            serverWorker.DoWork += (s, e) =>
            {
                Console.WriteLine("DoWork");

                using (var server = CreateWebServer(url))
                {
                    server.RunAsync();
                    serverWorker.ReportProgress(100);
                    while (serverWorker.CancellationPending == false)
                    {
                        System.Threading.Thread.Sleep(100);
                    }
                }
                serverWorker.ReportProgress(0);
            };

            serverWorker.RunWorkerCompleted += (s, e) =>
            {
               Console.WriteLine("RunWorkerCompleted");
            };

            serverWorker.RunWorkerAsync();
        }

        private static WebServer CreateWebServer(string url)
        {

            Console.WriteLine("Startup Path {0}", Application.StartupPath);
            string appPath = Application.StartupPath;
            string certFile = $"{appPath}\\WILDCARD_dukapc_dk.pfx";
            var server = new WebServer(o => o
                    .WithUrlPrefix(url)
                    .WithCertificate(new X509Certificate2(certFile, "1234"))
                    .WithAutoLoadCertificate()
                    .WithMode(HttpListenerMode.EmbedIO))
                .WithLocalSessionManager()
                .WithWebApi("/api", m => m  
                    .WithController<DixaController>())
                .WithModule(new ActionModule("/", HttpVerbs.Any, ctx => ctx.SendDataAsync(new { Message = "Error" })));

            // Listen for state changes.
            server.StateChanged += (s, e) => $"WebServer New State - {e.NewState}".Info();
            return server;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            serverWorker.CancelAsync();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
