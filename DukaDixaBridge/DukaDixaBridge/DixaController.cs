﻿using EmbedIO;
using EmbedIO.Actions;
using EmbedIO.Routing;
using EmbedIO.WebApi;
using Swan;
using System.Web;
using System.Web.UI.WebControls.WebParts;

namespace DukaDixaBridge
{
    public sealed class DixaController : WebApiController
    {
        [Route(HttpVerbs.Get, "/phone/{id?}")]
        public string GetPhone(string id) => Dixa.GetCrmAsync(id);
        protected override void OnBeforeHandler()
        {
            base.OnBeforeHandler();
            Response.Headers.Add(HttpHeaderNames.AccessControlAllowOrigin, "*");
        }

    }
}
