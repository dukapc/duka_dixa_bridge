﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using Swan.Logging;


namespace DukaDixaBridge
{
    public class Dixa
    {
        internal static string GetCrmAsync(string Id)
        {

            string phone = Id.Replace("+45", "");
            Console.WriteLine($"phone : {phone}");
            string constr = "AuthType=AD;Url=https://crm.dukapc.dk/dukaPC;";
            CrmServiceClient _service = new CrmServiceClient(constr);
           
            if (_service.IsReady)
            {
                try
                {
                    string fetchQuery = $@"
                            <fetch output-format='xml-platform' mapping='logical' version='1.0' >
                                <entity name = 'contact' >
                                <attribute name = 'lastname' alias = 'lastname' />
                                <attribute name = 'firstname' alias = 'firstname' />
                                <attribute name = 'contactid' alias = 'contactid' />
                                <attribute name = 'createdon' />
                                <order attribute = 'createdon' descending = 'true' />
                                <filter type = 'and' >
                                   <condition attribute = 'statecode' operator= 'eq' value = '0' />
                                   <filter type = 'or' >
                                     <condition attribute = 'telephone1' operator= 'eq' value = '{phone}' />
                                     <condition attribute = 'telephone2' operator= 'eq' value = '{phone}' />
                                     <condition attribute = 'mobilephone' operator= 'eq' value = '{phone}' />
                                   </filter >
                                </filter >
                                </entity >
                             </fetch >
                                " ;
                    
                    EntityCollection aCollection = _service.RetrieveMultiple(new FetchExpression(fetchQuery));
                    if (aCollection.Entities.Count > 0)
                    {
                        var oContact = aCollection.Entities[0];
                        Console.WriteLine("aCollection: {0}", oContact["contactid"]);
                        var contactid = oContact["contactid"];
                        string myurl = $"https://crm.dukapc.dk/dukaPC/main.aspx?etn=contact&id={contactid}&newWindow=true&pagetype=entityrecord";

                        var browser = new System.Diagnostics.Process()
                        {
                            StartInfo = new System.Diagnostics.ProcessStartInfo(myurl) { UseShellExecute = true }
                        };
                        browser.Start();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            } 
            _service.Dispose();
            return "ok service:";
        }
    }
}
